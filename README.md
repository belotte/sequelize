# Sequelize

### Postgres
Creer un utilisateur
``` sql
CREATE USER <utilisateur>;
```
Donner les droits SUPERUSER a un utilisateur
``` sql
ALTER USER <utilisateur> WITH SUPERUSER;
```
Ajouter/modifier le mot de passe d'un utilisateur
``` sql
\password <utilisateur>
```
Autoriser l'acces distant aux databases
``` sh
sudo sed -ie "s/#listen_addresses = 'localhost'/listen_addresses = '*'/" /etc/postgresql/*/main/postgresql.conf
sudo sed -ie "s/host *all *all *127\.0\.0\.1\/32 *md5/host     all     all     0.0.0.0\/0     md5/" /etc/postgresql/*/main/pg_hba.conf
```
